# Git training

This project is a sandbox used in the "Git for Beginners" training, to get hands-on with basic git functionality.

## Exercise: 

1. Clone the repository, and checkout the `main` branch.
2. Create your own branch.
3. Edit the file `me.md` and replace the name with yours. 
4. Push your branch. 
5. Create a merge request to merge it into someone else's branch.
6. Resolve conflicts (if any), and make the merge button turn blue!
